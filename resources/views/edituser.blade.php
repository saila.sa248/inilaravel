@include('header')

<div class="container-fluid float-left">
    <div class="row animated--grow-in">
        <div class="col-xl-6 col-lg-7">
            <div class="card shadow mb-4 ">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between mb-3 bg-gradient-info">
                    <h6 class="m-0 font-weight-bold text-white">PROFILE ADMIN</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                            aria-labelledby="dropdownMenuLink">
                            @foreach($userprofile as $up)
                            <div class="dropdown-header">Pilihan Anda :</div>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item text-info" href="{{ route('tuser') }}">Tambah Admin Baru</a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <form action="#" method="GET">
                    @foreach($userprofile as $up)
                    <div class="col-md-12 mb-3">
                        <label class="ml-2"><b>ID</b></label>
                        <input type="text" name="id" value="{{ $up->id }}" class="form-control bg-light border-1 small"
                            aria-describedby="basic-addon2" readonly>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label class="ml-2"><b>Nama</b></label>
                        <input type="text" name="nama" value="{{ $up->nama }}"
                            class="form-control bg-light border-1 small" aria-describedby="basic-addon2" readonly>
                    </div>

                    <div class="col-md-12 mb-3">
                        <label class="ml-2"><b>Alamat</b></label>
                        <input type="text" name="alamat" value="{{ $up->alamat }}"
                            class="form-control bg-light border-1 small" aria-describedby="basic-addon2" readonly>
                    </div>

                    <div class="col-md-12 mb-3">
                        <label class="ml-2"><b>Tanggal Lahir</b></label>
                        <input type="text" name="tgl_lahir" value="{{ $up->tgl_lahir }}"
                            class="form-control bg-light border-1 small" aria-describedby="basic-addon2" readonly>
                    </div>

                    <div class="col-md-12 mb-3">
                        <label class="ml-2"><b>E-mail</b></label>
                        <input type="text" name="email" value="{{ $up->email }}"
                            class="form-control bg-light border-1 small" aria-describedby="basic-addon2" readonly>
                    </div>

                    <div class="col-md-12 mb-3">
                        <label class="ml-1"><b>Password</b></label>
                        <div class="input-group">
                            <input type="password" name="password" value="{{ $up->password }}" data-toggle="password"
                                id="password" class="form-control bg-light border-1 small"
                                aria-describedby="basic-addon2" readonly>
                            <div class="input-group-append">
                                <button class="input-group-text">
                                    <i class="fa fa-eye"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </form>
            </div>
        </div>

        <!-- FORM EDIT USER -->

        <div class="col-xl-6 col-lg-7">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between mb-3">
                    <h6 class="m-0 font-weight-bold text-info">EDIT ADMIN</h6>
                </div>
                <form action="{{ route('upuser') }}" method="post">
                    @foreach($userprofile as $up)
                    {{csrf_field()}}
                    <div class="col-md-12 mb-3">
                        <label class="ml-2"><b>ID</b></label>
                        <input type="text" name="id" require="id" id="id" value="{{ $up->id }}"
                            class="form-control bg-light border-1 small @error('id') is-invalid @enderror"
                            aria-describedby="basic-addon2" readonly>
                        @error('id')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-12 mb-3">
                        <label class="ml-2"><b>Nama</b></label>
                        <input type="text" name="nama" value="{{ $up->nama }}" require="nama" id="nama"
                            class="form-control bg-light border-1 small  @error('nama') is-invalid @enderror"
                            aria-describedby="basic-addon2">
                        @error('nama')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="col-md-12 mb-3">
                        <label class="ml-2"><b>Alamat</b></label>
                        <input type="text" name="alamat" value="{{ $up->alamat }}" require="alamat" id="alamat"
                            class="form-control bg-light border-1 small @error('nama') is-invalid @enderror"
                            aria-describedby="basic-addon2">
                        @error('alamat')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="col-md-12 mb-3">
                        <label class="ml-2"><b>Tanggal Lahir</b></label>
                        <input type="text" name="tgl_lahir" value="{{ $up->tgl_lahir }}" require="tgl_lahir"
                            id="tgl_lahir"
                            class="form-control bg-light border-1 small @error('tgl_lahir') is-invalid @enderror"
                            aria-describedby="basic-addon2">
                        @error('tgl_lahir')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="col-md-12 mb-3">
                        <label class="ml-2"><b>E-mail</b></label>
                        <input type="text" name="email" value="{{ $up->email }}" require="email" id="email"
                            class="form-control bg-light border-1 small @error('email') is-invalid @enderror"
                            aria-describedby="basic-addon2">
                        @error('email')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="col-md-12 mb-3">
                        <label class="ml-1"><b>Password</b></label>
                        <div class="input-group">
                            <input type="password" name="password" value="{{ $up->password }}" data-toggle="password"
                                id="password" require="password"
                                class="form-control bg-light border-1 small @error('email') is-invalid @enderror"
                                aria-describedby="basic-addon2">
                            @error('password')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <div class="input-group-append">
                                <button class="input-group-text">
                                    <i class="fa fa-eye"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group float-right mt-3 mr-3">
                        <button class="btn btn-sm btn-danger" type="reset">Reset</button>
                        <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                    </div>
                    @endforeach
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="{!! asset('assets/vendor/jquery/jquery.min.js') !!}"></script>
<script src="{!! asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>

<!-- Core plugin JavaScript-->
<script src="{!! asset('assets/vendor/jquery-easing/jquery.easing.min.js') !!}"></script>

<!-- Custom scripts for all pages-->
<script src="{!! asset('assets/js/sb-admin-2.min.js') !!}"></script>