@include('header')

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Blog/Artikel</h1>
        <a href="{{ route('cetak_laporan_blog') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>
    <!-- Content Row -->
    <div class="card shadow mb-4">
        <a href="#collapseCardExample" class="d-block card-header py-3 bg-warning" data-toggle="collapse" role="button"
            aria-expanded="true" aria-controls="collapseCardExample">
            <h6 class="m-0 font-weight-bold text-white">Tambah Artikel</h6>
        </a>
        <div class="collapse hide" id="collapseCardExample">
            <div class="card-body">
                <form action="{{ url('blog') }}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class=" row">
                        <div class="col col-md-3">
                            <label for="name">JUDUL ARTIKEL</label>
                            <input class="form-control @error('judulartikel') is-invalid @enderror" type="text"
                                name="judulartikel" id="judulartikel" require="judulartikel"
                                value="{{ old('judulartikel') }}" placeholder="Masukkan Judul Artikel">
                            @error('judulartikel')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col col-md-4">
                            <label for="type">ISI ARTIKEL</label>
                            <textarea class="form-control @error('isiartikel') is-invalid @enderror" type="text"
                                name="isiartikel" id="isiartikel" require="isiartikel" value="{{ old('isiartikel') }}"
                                placeholder="Masukan Isi Artikel"></textarea>
                            @error('isiartikel')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col col-md-3">
                            <label for="name">AUTHOR</label>
                            <input class="form-control @error('author') is-invalid @enderror" type="text" name="author"
                                id="author" require="author" value="{{ old('author') }}" placeholder="Penulis/author">
                            @error('author')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col col-md-2">
                            <label for="date">TANGGAL DIBUAT</label>
                            <input class="form-control @error('date') is-invalid @enderror" type="date" name="date"
                                id="date" require="date" value="{{ old('date') }}">
                            @error('date')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group float-right mt-4">
                        <button class="btn btn-sm btn-danger" type="reset">Reset</button>
                        <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <form action="{{ route('cariartikel') }}" method="GET"
        class="d-none d-sm-inline-block form-inline mr-auto mb-3 navbar-search">
        <div class="input-group">
            <input type="text" name="cari" value="{{ old('cari') }}" class="form-control bg-light border-1 small"
                placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
            <div class="input-group-append">
                <button class="btn btn-warning" type="submit">
                    <i class="fas fa-search fa-sm"></i>
                </button>
            </div>
        </div>
    </form>
    <div class="col-12">
        @if (session('status'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
    </div>

    <style>
    #more {
        display: none;
    }
    </style>

    <div class="row">
        <div class="col-xl-12 animated--grow-in">
            <div class="card shadow h-100 py-2">
                <div class="card-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead class="bg-dark text-white">
                            <tr>
                                <th class="row-md-1" scope="col">ID</th>
                                <th class="row-md-1" scope="col">AUTHOR</th>
                                <th class="row-md-2" scope="col">JUDUL ARTIKEL</th>
                                <th class="row-md-3" scope="col">ISI ARTIKEL</th>
                                <th class="row-md-2" scope="col">TANGGAL DIBUAT</th>
                                <th class="row-md-2" scope="col">TANGGAL DIUBAH</th>
                                <th class="row-md-1" scope="col">OPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($blogcontrol as $blog)
                            <tr>
                                <td>
                                    {{$blog -> id}}
                                </td>
                                <td>
                                    {{$blog -> author}}
                                </td>
                                <td>
                                    {{$blog -> judulartikel}}
                                </td>
                                <td>
                                    {{$blog->isiartikel}}
                                </td>
                                <td>
                                    {{$blog -> created_at}}
                                </td>
                                <td>
                                    {{$blog -> updated_at}}
                                </td>
                                <td class="td-actions text-right">
                                    <a href="editblog/edit/{{ $blog-> id}}"
                                        class="d-none d-sm-inline-block btn btn-sm btn-success mb-2">
                                        <i class="fas fa-pen fa-sm text-black mr-1"></i> Ubah</a>
                                    <a href="{{ route('blogcontrol.destroy', $blog->id) }}" data-toggle="modal"
                                        title="Remove" data-target="#hapusModal"
                                        class="d-none d-sm-inline-block btn btn-sm btn-danger">
                                        <i class="fas fa-trash-alt fa-sm text-black mr-1"></i>Hapus</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-warning" href="{{ route('logout')}}">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Hapus Modal-->
<div class="modal fade" id="hapusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Yakin Mau Dihapus?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Pilih "HAPUS" untuk menghapus data.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <a class="btn btn-warning" href="{{ route('blogcontrol.destroy', $blog->id) }}">Hapus</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="{!! asset('assets/vendor/jquery/jquery.min.js') !!}"></script>
<script src="{!! asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>

<!-- Core plugin JavaScript-->
<script src="{!! asset('assets/vendor/jquery-easing/jquery.easing.min.js') !!}"></script>

<!-- Custom scripts for all pages-->
<script src="{!! asset('assets/js/sb-admin-2.min.js') !!}"></script>

</body>

</html>