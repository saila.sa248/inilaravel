<!doctype html>
<html lang="en">

<head>
    <style>
    html {
        scroll-behavior: smooth;
        scroll-padding-top: -50px;
    }
    </style>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,500;0,600;1,500&display=swap"
        rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- CSS UTAMA -->
    <link rel="stylesheet" href="{!! asset('assets/css/ini.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/style-blog.css') !!}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- <link rel="javascript" href="js/script.js"> -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>


    <title>Diginusa</title>
</head>

<body>
    <!-- START NAVBAR -->
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
            <a class="navbar-brand" href="#" style="font-size:20px;">
                <img src="{!! asset('assets/img/diginusa1.png')!!}" width="35" height="35">
                Diginusa</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#service">Service</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('halblog') }}">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">contact</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @foreach($tampilslider as $slide)
                <li data-target="#carouselExampleIndicators" data-slide-to="{{ $slide->id }}" class="active"></li>
                @endforeach
            </ol>
            <div class="carousel-inner">
                @foreach($tampilslider as $key => $slide)
                <div class="carousel-item {{ $key == 0 ? ' active' : ''}}">
                    <img src="{!! asset('assets/img/'.$slide->file) !!}" width="2000px" height="800px">
                    <div class="carousel-caption">
                        <h2>We are <span>Faster</span><br>and<span> Better</span></h2>
                        <p><a href="#about">Get Started</a></p>
                    </div>
                </div>
                @endforeach

            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </header>
    <!-- END NAVBAR -->

    <!-- AWAL BEKGRON -->
    <style>
    .bekgron {
        background-image:url('{!! asset('assets/img/ooooo.png') !!}');
        background-size: cover;
    }
    </style>
    <div class="bekgron">
        <div class="row">
            <!-- AWAL How Do We Work -->
            <div class="col-lg-12 col-md-12 col-sm-12 about" id="about">
                <div class="text-center">
                    <h4 style="font-weight:bolder; margin-left:10px; padding-bottom: 30px;">
                        <img src="{!! asset('assets/img/about1.png') !!}" width=100px; height=100px;>How Do We Work ?
                    </h4>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-6 p-5">
                        <div class="row">
                            <!-- BIARKAN KOSONG -->
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 text-left pr-5">
                        <div class="row">
                            @foreach($tampilteks as $tteks)
                            <div class="col-md-6">
                                <img class="mr-3" src="{!! asset('assets/img/data_fileteks/'.$tteks->file) !!}"
                                    width=50px; height=50px; style="float:left;"><br>
                                <h5>{{ $tteks -> nama}}</h5>
                                <p class="text-lowercase">{{ $tteks->isiteks}}</p>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!-- END How Do We Work -->

            <!-- START SERVICE -->
            <div class="col-lg-12 col-md-12 col-sm-12 service mt-10" id="service">

                <div class="col-lg-12 font-weight-bold mb-5">
                    <label for="Service"> OUR SERVICES </label>
                </div>

                <div class="card-group pt-5">

                    <div class="col-md-3 container-fluid">
                        <div class="card">
                            <div class="con-img">
                                <img src="{!! asset('assets/img/srv1.png')!!}" alt="Card image cap">
                                <img class="blur" src="{!! asset('assets/img/srv1.png')!!}">
                            </div>

                            <div class="card-body">
                                <h4 class="card-text">Website & Mobile Development</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 container-fluid">
                        <div class="card">
                            <div class="con-img">
                                <img src="{!! asset('assets/img/develop.png')!!}" alt="Card image cap">
                                <img class="blur" src="{!! asset('assets/img/develop.png')!!}">
                            </div>
                            <div class="card-body">
                                <h4 class="card-text">UI & UX Design</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 container-fluid">
                        <div class="card">
                            <div class="con-img">
                                <img src="{!! asset('assets/img/srv3.png')!!}" alt="Card image cap">
                                <img class="blur" src="{!! asset('assets/img/srv3.png')!!}">
                            </div>
                            <div class="card-body">
                                <h4 class="card-text">Digital Marketing</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 container-fluid">
                        <div class="card">
                            <div class="con-img">
                                <img src="{!! asset('assets/img/wordpress1.png')!!}" alt="Card image cap">
                                <img class="blur" src="{!! asset('assets/img/wordpress1.png')!!}">
                            </div>
                            <div class="card-body">
                                <h4 class="card-text">Full Custom Wordpress</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END SERVICE -->

            <!-- START MADE WITH LOVE -->
            <div class="col-lg-12 col-md-12 col-sm-12 madewithlove text-center mb-5">
                <label for="Service" class="font-weight-bold text-gray-500">MADE WITH LOVE</label>
                <div class="row content-center mt-5">
                    <div class="d-flex justify-content-center mb-5 pb-5">
                        <div class="col-lg-6 col-md-6 col-xs-6">
                            @foreach($tampilportfolio as $tportfolio)
                            <img src=" {!! asset('assets/img/portfolio/'.$tportfolio->file) !!}" class="slideih">
                            @endforeach

                            <button onclick="plusDivs(-1)" class="btnpf"><i class="fa fa-chevron-right"></i></button>
                            <button onclick="plusDivs(1)" class="btnpf"><i class="fa fa-chevron-left"></i></button>
                        </div>
                    </div>
                    <!-- <div class="col-lg-4 col-md-4 col-ls-4"></div> -->
                </div>

                <script>
                var slideIndex = 1;
                showDivs(slideIndex);

                function plusDivs(n) {
                    showDivs(slideIndex += n);
                }

                function showDivs(n) {
                    var i;
                    var x = document.getElementsByClassName("slideih");
                    if (n > x.length) {
                        slideIndex = 1
                    }
                    if (n < 1) {
                        slideIndex = x.length
                    }
                    for (i = 0; i < x.length; i++) {
                        x[i].style.display = "none";
                    }
                    x[slideIndex - 1].style.display = "block";
                }
                </script>
            </div>
            <!-- END MADE WITH LOVE -->

        </div>
    </div>
    <!-- AKHIR BEKGRON -->

    <div class="row">

        <!-- START CONTACT -->
        <style>
        .contact {
            background-image: url('{!! asset('assets/img/bg-06.png') !!}');
            background-size: cover;
        }
        </style>

        <div class="col-lg-12 col-md-12 col-sm-12 bg-dark text-white">
            <div class="text-center mt-5 pt-5">
                MADE WITH LOVE
            </div>

            <div class="row">
                <!-- FORM KONTAK -->
                <div class="col-md-6 align-content-between p-5 ">
                    <form action="{{ route('kirimpesan') }}" method="post" class="text-left">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Email address</label>
                            <input type="email" name="pengirim" require="pengirim"
                                class="form-control @error('pengirim') is-invalid @enderror" id="pengirim"
                                placeholder="name@example.com">
                            @error('pengirim')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Message</label>
                            <textarea class="form-control @error('isipesan') is-invalid @enderror" name="isipesan"
                                require="isipesan" id="exampleFormControlTextarea1" rows="6"></textarea>
                        </div>
                        <div class="form-group float-right">
                            <button class="btn btn-sm btn-warning pl-3 pr-3 pt-1 pb-1" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- END OF KONTAK -->

                <div class="col-md-6 text-left p-5">
                    <h2 class="font-weight-bold text-warning text-uppercase">Diginusa Studio</h2>
                    <p>Hey There! Contact us for more information</p>
                </div>
            </div>
        </div>
        <!-- END CONTACT -->


        <!-- START FOOTERCONTACT -->
        <style>
        .footercontact {
            background-image: url('{!! asset('assets/img/bg-06.png') !!}');
            background-size: cover;
        }
        </style>

        <div class="col-lg-12 col-md-12 col-sm-12 bg-dark">
            <div class="bg-bawah">

                <div id="bawah">
                    <div id="kiri-footer">
                        <div class="row">
                            <div class="col" style="border-right:1px solid grey;">
                                <img src="{!! asset('assets/img/logo.png') !!}" id="imgfooter">
                            </div>
                            <div class="col" style="color:grey; font-style:italic;">
                                <a> software enterprise solutions</a>
                            </div>
                        </div>
                    </div>

                    <div id="kanan-footer">
                        <a> Hubungi Kami</a><br>
                        <a> Jln. Mekar Indah 14 Bandung</a><br>
                        <a> hi@diginusastudio.com</a><br>
                        <a> +6289666905702</a><br>
                        <a href="#"><img src="{!! asset('assets/img/icon/facebook.png') !!}" href="#" alt="img"
                                width="30" height="30"></a>
                        <a href="#"><img src="{!! asset('assets/img/icon/instagram.png') !!}" href="#" width="30"
                                height="30"></a>
                        <a href="#"><img src="{!! asset('assets/img/icon/linkedin.png') !!}" href="#" width="30"
                                height="30"></a>
                    </div>
                    <div class="tl-ds"> Diginusa merupakan salah satu perusahaan <br> yang begerak di bidang IT.
                    </div>
                </div>
                <!-- END OF FOOTER CONTACT -->

                <footer class="pb-3 font-weight-normal">
                    Copyrightby@<span>DiginusaStudio2020</span>
                </footer>
            </div>
        </div>
    </div>
    <!-- AKHIR FOOTER -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>