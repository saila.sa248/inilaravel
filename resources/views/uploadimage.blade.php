@include('header')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Image Slider</h1>
    </div>
    <!-- Content Row -->
    <div class="card shadow mb-4">
        <a href="#collapseCardExample" class="d-block card-header py-3 bg-warning" data-toggle="collapse" role="button"
            aria-expanded="true" aria-controls="collapseCardExample">
            <h6 class="m-0 font-weight-bold text-white">Tambah Data Image</h6>
        </a>
        <div class="collapse hide" id="collapseCardExample">
            <div class="card-body">
                <form action="{{ url('image') }}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col col-md-6">
                            <label for="name">FILE IMAGE</label>
                            <!-- <input class="form-control btn btn-success @error('image') is-invalid @enderror" type="file"
                                name="image" id="image" require="image" value="{{ old('image') }}"> -->
                            <div class="custom-file">
                                <input type="file" class="custom-file-input @error('file') is-invalid @enderror"
                                    id="file" name="file" require="file" value="{{ old('file')}}">
                                <label class="custom-file-label" for="file">Pilih Gambar..</label>
                                @error('file')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <script>
                            // Add the following code if you want the name of the file appear on select
                            $(".custom-file-input").on("change", function() {
                                var fileName = $(this).val().split("\\").pop();
                                $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                            });
                            </script>
                        </div>
                        <div class="col col-md-6">
                            <label for="type">KETERANGAN</label>
                            <input class="form-control @error('keterangan') is-invalid @enderror" type="text"
                                name="keterangan" id="keterangan" require="keterangan" value="{{ old('keterangan') }}"
                                placeholder="Masukan Keterangan">
                            @error('keterangan')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
            </div>
            <div class="form-group float-right mt-4">
                <button class="btn btn-sm btn-danger" type="reset">Reset</button>
                <button class="btn btn-sm btn-primary" type="submit">Upload</button>
            </div>
            </form>

        </div>
    </div>

    <div class="col-12">
        @if (session('status'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
    </div>

    <div class="row">
        <div class="col-xl-12 animated--grow-in">
            <div class="card shadow h-100 py-2">
                <div class="card-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead class="bg-dark text-white">
                            <tr>
                                <th class="row-md-0" scope="col">ID</th>
                                <th class="row-md-3" scope="col">FILE IMAGE</th>
                                <th class="row-md-3" scope="col">KETERANGAN</th>
                                <th class="row-md-2" scope="col">TANGGAL DIBUAT</th>
                                <th class="row-md-2" scope="col">TANGGAL DIUBAH</th>
                                <th class="row-md-2" scope="col">OPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($gambar as $up)
                            <tr>
                                <td>
                                    {{$up -> id}}
                                </td>
                                <td>
                                    <img width="20%" src="{!! asset('assets/img/'.$up->file) !!}">
                                </td>
                                <td>
                                    {{$up -> keterangan}}
                                </td>
                                <td>
                                    {{$up -> created_at}}
                                </td>
                                <td>
                                    {{$up -> updated_at}}
                                </td>
                                <td class="td-actions text-right">
                                    <a href="editimage/edit/{{ $up-> id}}"
                                        class="d-none d-sm-inline-block btn btn-sm btn-success mb-2">
                                        <i class="fas fa-pen fa-sm text-black mr-1"></i> Ubah</a>
                                    <a href="{{ route('imagecontrol.destroy', $up->id) }}" data-toggle="modal"
                                        title="Remove" data-target="#hapusModal"
                                        class="d-none d-sm-inline-block btn btn-sm btn-danger">
                                        <i class="fas fa-trash-alt fa-sm text-black mr-1"></i>Hapus</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->


<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-warning" href="{{ route('logout')}}">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Hapus Modal-->
<div class="modal fade" id="hapusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Yakin Mau Dihapus?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Pilih "HAPUS" untuk menghapus data.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                <a class="btn btn-warning" href="{{ route('imagecontrol.destroy', $up->id) }}">Hapus</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="{!! asset('assets/vendor/jquery/jquery.min.js') !!}"></script>
<script src="{!! asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>

<!-- Core plugin JavaScript-->
<script src="{!! asset('assets/vendor/jquery-easing/jquery.easing.min.js') !!}"></script>

<!-- Custom scripts for all pages-->
<script src="{!! asset('assets/js/sb-admin-2.min.js') !!}"></script>

</body>

</html>