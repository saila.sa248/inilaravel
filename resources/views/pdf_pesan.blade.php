<!DOCTYPE html>
<html>

<head>
    <title>Laporan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <center>
        <h3>Laporan Data Pesan Masuk</h3>
    </center>
    <br><br>
    <table class="table table-bordered text-center text-small">
        <thead class="bg-dark text-white">
            <tr>
                <th>ID</th>
                <th>Pengirm</th>
                <th>Isi Pesan</th>
            </tr>
        </thead>
        <tbody>
            @foreach($pesan as $p)
            <tr>
                <td>{{$p->id}}</td>
                <td>{{$p->pengirim}}</td>
                <td>{{$p->isipesan}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

</body>

</html>