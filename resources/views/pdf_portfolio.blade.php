<!DOCTYPE html>
<html>

<head>
    <title>Laporan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <center>
        <h3>Laporan Data Portfolio</h3>
    </center>
    <br><br>
    <table class="table table-bordered text-center">
        <thead class="bg-dark text-white">
            <tr>
                <th>ID</th>
                <th>Nama File</th>
                <th>Keterangan</th>
                <th>Tanggal Dibuat</th>
                <th>Tanggal Diubah</th>
            </tr>
        </thead>
        <tbody>
            @foreach($portfolio as $p)
            <tr>
                <td>{{$p->id}}</td>
                <td>{{$p->file}}</td>
                <td>{{$p->keterangan}}</td>
                <td>{{$p->created_at}}</td>
                <td>{{$p->updated_at}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

</body>

</html>