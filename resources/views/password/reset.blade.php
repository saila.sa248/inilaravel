<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="{!! asset('assets/css/style.css') !!}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reset</title>
</head>

<body>
    <div class="container">
        <form method="POST" action="{{ route('resetpass') }}">
            @if (session('pesan'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('pesan') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group">
                <img src="{!! asset('assets/img/logo.png')!!}"><br><br><br>
                <input id="email" type="email" class="form-control-user @error('email') is-invalid @enderror"
                    name="email" value="{{ $email ?? old('email') }}" autocomplete="email" autofocus
                    placeholder="E-mail Adress">

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

                <input id="password" type="password" class="form-control-user @error('password') is-invalid @enderror"
                    name="password" autocomplete="new-password" placeholder="Password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

                <input id="password-confirm" type="password" class="form-control-user" name="password_confirmation"
                    autocomplete="new-password" placeholder="Confirm Password">

                <button type="submit" class="btn btn-primary  pl-5 pr-5 mt-1">
                    Reset Password
                </button>
            </div>
        </form>
    </div>

</body>

</html>