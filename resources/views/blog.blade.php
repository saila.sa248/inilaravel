<!doctype html>
<html lang="en">

<head>
    <style>
    html {
        scroll-behavior: smooth;
        scroll-padding-top: -50px;
    }
    </style>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="{!! asset('assets/css/ini.css') !!}"> -->
    <link rel="stylesheet" href="{!! asset('assets/css/style-blog.css') !!}">

    <!-- script -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
        integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous">
    </script>

    <title>NEWS!!</title>
</head>

<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
            <a class="navbar-brand" href="#" style="font-size:20px;">
                <img src="{!! asset('assets/img/diginusa1.png') !!}" width="35" height="35">
                Diginusa</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('welcome')}}">Website <span
                                class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Home</a>
                    </li>
                </ul>
            </div>
        </nav>

        <style>
        .jumbotron {
            background-image: url('{!! asset('assets/img/lat1.jpg') !!}');
            background-size: cover;
        }
        </style>

        <div class="jumbotron jumbotron-fluid">
            <div class="container-jum text-center">
                <!-- <img src="img/diginusa1.png" width="300" class="rounded-circle"> -->
                <h1 class="display-4">We Are Certified Engineers</h1>
                <p class="lead" style="margin-bottom:20px;">Solution for Keeping Business</p>
                <p style="color:white; text-align:left;"> Apa yang anda butuhkan kami sediakan, percayakan saja pada
                    team kami <br> Kepuasan anda kebangga kami </p>
                <p><a href="#kiri" class="btn btn-warning">Read More</a></p>
            </div>
        </div>
    </header>

    <div id="wrapper" id="pertama">
        <div id="kiri">
            <!-- card start -->
            @foreach($blog as $b)
            <div class="card-kanan">
                <img class="con-img" src="{!! asset('assets/img/news1.png') !!}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">{{ $b->judulartikel}}</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                        the card's content.</p>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalLong">
                        Read More
                    </button>
                </div>
            </div>
            <!-- card end -->
            <!-- Modal -->
            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Diginusa</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>{{$b->isiartikel}}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- akhir modal -->
            <!-- card start
            <div class="card-kanan">
                <img class="con-img" src="{!! asset('assets/img/news2.png') !!}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                        the card's content.</p>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalLong">
                        Read More
                    </button>
                </div>
            </div> -->
            <!-- card end -->
            <!-- Modal -->
            <!-- <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Diginusa</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                        </div>s
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- akhir modal -->
            <!-- card start -->
            <!-- <div class="card-kanan">
                <img class="con-img" src="{!! asset('assets/img/news4.png') !!}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                        the card's content.</p>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalLong">
                        Read More
                    </button>
                </div>
            </div> -->
            <!-- card end -->
            <!-- Modal -->
            <!-- <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Diginusa</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                        </div>s
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- akhir modal -->
            <!-- card start -->
            <!-- <div class="card-kanan">
                <img class="con-img" src="{!! asset('assets/img/news5.png') !!}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                        the card's content.</p>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalLong">
                        Read More
                    </button>
                </div>
            </div> -->
            <!-- card end -->
            <!-- Modal -->
            <!-- <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Diginusa</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                        </div>s
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- akhir modal -->
            <!-- card start -->
            <!-- <div class="card-kanan">
                <img class="con-img" src="{!! asset('assets/img/news6.png') !!}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                        the card's content.</p>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalLong">
                        Read More
                    </button>
                </div>
            </div> -->
            <!-- card end -->
            <!-- Modal -->
            <!-- <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Diginusa</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                        </div>s
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- akhir modal -->
            <!-- card start -->
            <!-- <div class="card-kanan">
                <img class="con-img" src="{!! asset('assets/img/news7.png') !!}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                        the card's content.</p>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalLong">
                        Read More
                    </button>
                </div>
            </div> -->
            <!-- card end -->
            <!-- Modal -->
            <!-- <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Diginusa</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis repudiandae
                                voluptatibus similique
                                nesciunt. Ipsum iure recusandae accusantium nihil est? Cupiditate commodi minus
                                dolore a quod
                                doloribus voluptatem iure repellendus sed!</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- akhir modal -->
            <!-- <div class="more">
                <p><a href="#" class="btnjum">More</a></p>
            </div> -->
        </div>
        <!--end of kiri-->
        <div id="kanan">
            <div class="card-kiri bg-light mb-3" style="max-width: 18rem;">
                <div class="card-header">NEWS</div>
                <div class="card-body">
                    <a href="{{ route('news1') }}">NEWS-01</a><br>
                    <a href="{{ route('news2') }}">NEWS-02</a><br>
                    <a href="{{ route('news3') }}">NEWS-03</a><br>
                    <a href="{{ route('news4') }}">NEWS-04</a><br>
                    <a href="{{ route('news5') }}">NEWS-05</a><br>
                    <a href="{{ route('news6') }}">NEWS-06</a><br>
                    <a href="{{ route('news7') }}">NEWS-07</a><br>
                    <a href="{{ route('news8') }}">NEWS-08</a><br>
                    <a href="{{ route('news9') }}">NEWS-09</a><br>
                    <a href="{{ route('news10') }}">NEWS-10</a>
                </div>
            </div>
        </div>
        <!--end of kanan-->
        <div class="clear"></div>
    </div>
    <!--end of clear-->
    <div class="bg-bawah" id="bg-web">
        <div id="bawah">
            <div id="kiri-footer">
                <div class="row">
                    <div class="col" style="border-right:1px solid grey;">
                        <img src="{!! asset('assets/img/logo.png') !!}" id="imgfooter">
                    </div>
                    <div class="col" style="color:grey; font-style:italic;">
                        <a> software enterprise solutions</a>
                    </div>
                </div>
            </div>
            <div id="kanan-footer">
                <a> Hubungi Kami</a><br>
                <a> Jln. Mekar Indah 14 Bandung</a><br>
                <a> hi@diginusastudio.com</a><br>
                <a> +6289666905702</a><br>
                <a href="#"><img src="{!! asset('assets/img/icon/facebook.png') !!}" href="#" alt="img" width="30"
                        height="30"></a>
                <a href="#"><img src="{!! asset('assets/img/icon/instagram.png') !!}" href="#" width="30"
                        height="30"></a>
                <a href="#"><img src="{!! asset('assets/img/icon/linkedin.png') !!}" href="#" width="30"
                        height="30"></a>
            </div>
            <div class="tl-ds"> Diginusa merupakan salah satu perusahaan <br> yang begerak di bidang IT.</div>
        </div>
        <!--end of bawah-->
        <footer>
            <a>Copyright Diginusa 2020</a>
        </footer>
    </div>

    <!-- <div class="footer-blog">
            <a> Copyright by Diginusa 2020</a>
        </div> -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

</body>

</html>