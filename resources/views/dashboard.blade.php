@include('header')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Diginusa Dashboard</h1>
    </div>

    <!-- Content ATAS -->
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Earnings (Monthly)
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">$40,000</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Earnings (Annual)
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">$215,000</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Tasks</div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
                                </div>
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 50%"
                                            aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pending Requests
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">18</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Content Row -->
    <div class="row">
        <div class="col-md-6 animated--grow-in">
            <div class="card shadow mb-12">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between mb-3">
                    <h6 class="m-0 font-weight-bold text-dark">Tabel Data Teks</h6>
                </div>

                <form class="col-12">
                    <table class="table table-bordered table-hover table-striped">
                        <thead class="thead-dark text-white">
                            <tr>
                                <th scope="col" class="row-md-1">ID</th>
                                <th scope="col" class="row-md-1">JUDUL TEKS</th>
                                <th scope=" col" class="row-md-5">ISI TEKS</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($teks as $tks)
                            <tr>
                                <td>
                                    {{$tks-> id}}
                                </td>
                                <td>
                                    {{$tks-> nama}}
                                </td>
                                <td>
                                    {{$tks-> isiteks}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
        <div class="col-md-6 animated--grow-in">
            <div class="card shadow mb-12">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between mb-3">
                    <h6 class="m-0 font-weight-bold text-dark">Tabel Data Teks</h6>
                </div>

                <form class="col-12">
                    <table class="table table-bordered table-hover table-striped">
                        <thead class="thead-dark text-white">
                            <tr>
                                <th scope="col" class="row-md-1">ID</th>
                                <th scope="col" class="row-md-3">FILE</th>
                                <th scope=" col" class="row-md-6">KETERANGAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($image as $img)
                            <tr>
                                <td>
                                    {{$img-> id}}
                                </td>
                                <td>
                                    <img width="20%" src="{!! asset('assets/img/'.$img->file) !!}">
                                </td>
                                <td>
                                    {{$img-> keterangan}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6 col-lg-7 mt-4">
            <div class="card shadow mb-4 ">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between mb-3">
                    <h6 class="m-0 font-weight-bold text-dark">Tabel Portfolio</h6>
                </div>
                <form class="col-12">
                    <table class="table table-bordered table-hover table-striped" id="dataTables">
                        <thead class="thead-dark text-white">
                            <tr>
                                <th scope="col" class="row-md-1">ID</th>
                                <th scope="col" class="row-md-1">PORTFOLIO</th>
                                <th scope=" col" class="row-md-5">KETERANGAN</th>
                            </tr>
                        </thead>
                        @foreach($portfolio as $pf)
                        <tbody>
                            <tr>
                                <td>{{ $pf -> id }}</td>
                                <td>{{ $pf -> file }}</td>
                                <td>{{ $pf -> keterangan }}</td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>

                </form>
            </div>
        </div>
        <!-- End of Main Content -->

        <!-- Content Column -->
        <div class="col-xl-6 col-lg-7 mt-4">
            <!-- Project Card Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Projects</h6>
                </div>
                <div class="card-body">
                    <h4 class="small font-weight-bold">Server Migration <span class="float-right">20%</span></h4>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-danger" role="progressbar" style="width: 20%" aria-valuenow="20"
                            aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <h4 class="small font-weight-bold">Sales Tracking <span class="float-right">40%</span></h4>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-warning" role="progressbar" style="width: 40%" aria-valuenow="40"
                            aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <h4 class="small font-weight-bold">Customer Database <span class="float-right">60%</span></h4>
                    <div class="progress mb-4">
                        <div class="progress-bar" role="progressbar" style="width: 60%" aria-valuenow="60"
                            aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <h4 class="small font-weight-bold">Payout Details <span class="float-right">80%</span></h4>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80"
                            aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <h4 class="small font-weight-bold">Account Setup <span class="float-right">Complete!</span></h4>
                    <div class="progress">
                        <div class="progress-bar bg-success progress-bar-animated progress-bar-striped"
                            role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0"
                            aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; Your Website 2020</span>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-warning" href="{{ route('logout') }}">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{!! asset('assets/vendor/jquery/jquery.min.js') !!}"></script>
    <script src="{!! asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{!! asset('assets/vendor/jquery-easing/jquery.easing.min.js') !!}"></script>
    <script src="{!! asset('assets/vendor/datatables/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('assets/vendor/datatables/dataTables.bootstrap4.min.js') !!}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{!! asset('assets/js/sb-admin-2.min.js') !!}"></script>
    <script src="{!! asset('assets/js/demo/chart-area-demo.js')!!}"></script>
    <script src="{!! asset('assets/js/demo/chart-pie-demo.js')!!}"></script>
    </body>

    </html>