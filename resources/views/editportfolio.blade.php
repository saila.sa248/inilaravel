@include('header')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Portfolio</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>
    <!-- Content Row -->
    <div class="card shadow mb-4">
        <a href="#collapseCardExample" class="d-block card-header py-3 bg-warning" data-toggle="collapse" role="button"
            aria-expanded="true" aria-controls="collapseCardExample">
            <h6 class="m-0 font-weight-bold text-white">Ubah Portfolio</h6>
        </a>
        <div class="collapse show" id="collapseCardExample">
            <div class="card-body">
                @foreach($editportfolio as $edit)
                <form action="{{ route('upportfolio') }}" method="post">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col col-md-2">
                            <label for="name">ID</label>
                            <input class="form-control @error('id') is-invalid @enderror" type="text" name="id" id="id"
                                require="id" value="{{ $edit->id }}" readonly>
                            @error('id')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col col-md-3">
                            <label for="type">UPLOAD PORTFOLIO</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input @error('file') is-invalid @enderror"
                                    id="file" name="file" require="file" value="{{ $edit->file }}">
                                <label class="custom-file-label" for="file">Pilih Gambar..</label>
                                @error('file')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <script>
                            // Add the following code if you want the name of the file appear on select
                            $(".custom-file-input").on("change", function() {
                                var fileName = $(this).val().split("\\").pop();
                                $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                            });
                            </script>
                        </div>
                        <div class="col col-md-3">
                            <label for="type">KETERANGAN</label>
                            <input class="form-control @error('keterangan') is-invalid @enderror" type="text"
                                name="keterangan" id="keterangan" require="keterangan" value="{{ old('keterangan') }}"
                                placeholder="Masukan Keterangan">
                            @error('keterangan')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col col-md-2">
                            <label for="date">TANGGAL DIBUAT</label>
                            <input class="form-control @error('created_at') is-invalid @enderror" type="date"
                                name="created_at" id="created_at" require="created_at" value="{{ $edit->created_at }}"
                                readonly>
                            @error('created_at')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col col-md-2">
                            <label for="date">TANGGAL DIUBAH</label>
                            <input class="form-control @error('updated_at') is-invalid @enderror" type="date"
                                name="updated_at" id="updated_at" require="updated_at" value="{{ $edit->updated_at }}">
                            @error('updated_at')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group float-right mt-4">
                        <button class="btn btn-sm btn-danger" type="reset">Reset</button>
                        <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                    </div>
                </form>
                @endforeach
            </div>
        </div>
    </div>

    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-warning" href="{{ route('logout') }}">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="{!! asset('assets/vendor/jquery/jquery.min.js') !!}"></script>
<script src="{!! asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>

<!-- Core plugin JavaScript-->
<script src="{!! asset('assets/vendor/jquery-easing/jquery.easing.min.js') !!}"></script>

<!-- Custom scripts for all pages-->
<script src="{!! asset('assets/js/sb-admin-2.min.js') !!}"></script>

</body>

</html>