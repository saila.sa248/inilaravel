@include('header')

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Blog/Artikel</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>
    <!-- Content Row -->
    <div class="card shadow mb-4">
        <a href="#collapseCardExample" class="d-block card-header py-3 bg-warning" data-toggle="collapse" role="button"
            aria-expanded="true" aria-controls="collapseCardExample">
            <h6 class="m-0 font-weight-bold text-white">Ubah Artikel</h6>
        </a>
        <div class="collapse show" id="collapseCardExample">
            <div class="card-body">
                @foreach($editblog as $edit)
                <form action="{{ route('upblog') }}" method="post">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col col-md-1">
                            <label for="name">ID</label>
                            <input class="form-control @error('id') is-invalid @enderror" type="text" name="id" id="id"
                                require="id" value="{{ $edit->id }}" readonly>
                            @error('id')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col col-md-3">
                            <label for="name">JUDUL ARTIKEL</label>
                            <input class="form-control @error('judulartikel') is-invalid @enderror" type="text"
                                name="judulartikel" id="judulartikel" require="judulartikel"
                                value="{{ $edit->judulartikel }}" placeholder="Masukkan Judul Artikel">
                            @error('judulartikel')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col col-md-4">
                            <label for="type">ISI ARTIKEL</label>
                            <textarea class="form-control @error('isiartikel') is-invalid @enderror" type="text"
                                name="isiartikel" id="isiartikel" require="isiartikel" value="{{ $edit->isiartikel }}"
                                placeholder="Masukan Isi Artikel"></textarea>
                            @error('isiartikel')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col col-md-2">
                            <label for="type">AUTHOR</label>
                            <input class="form-control @error('author') is-invalid @enderror" type="text" name="author"
                                id="author" require="author" value="{{ $edit->author }}" placeholder="Penulis/Author">
                            @error('isiartikel')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col col-md-2">
                            <label for="date">TANGGAL DIUBAH</label>
                            <input class="form-control @error('updated_at') is-invalid @enderror" type="date"
                                name="updated_at" id="updated_at" require="updated_at" value="{{ $edit->updated_at }}">
                            @error('updated_at')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group float-right mt-4">
                        <button class="btn btn-sm btn-danger" type="reset">Reset</button>
                        <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                    </div>
                </form>
                @endforeach
            </div>
        </div>
    </div>

    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-warning" href="{{ route('logout') }}">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="{!! asset('assets/vendor/jquery/jquery.min.js') !!}"></script>
<script src="{!! asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>

<!-- Core plugin JavaScript-->
<script src="{!! asset('assets/vendor/jquery-easing/jquery.easing.min.js') !!}"></script>

<!-- Custom scripts for all pages-->
<script src="{!! asset('assets/js/sb-admin-2.min.js') !!}"></script>

</body>

</html>