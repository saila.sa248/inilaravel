<!DOCTYPE html>
<html>

<head>
    <title>Laporan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <center>
        <h3>Laporan Data Blog/Artikel</h3>
    </center>
    <br><br>
    <table class="table table-bordered text-center text-small">
        <thead class="bg-dark text-white">
            <tr>
                <th>ID</th>
                <th>Author</th>
                <th>Judul Artikel</th>
                <th>Isi Artikel</th>
                <th>Tanggal Dibuat</th>
                <th>Tanggal Diubah</th>
            </tr>
        </thead>
        <tbody>
            @foreach($blog as $b)
            <tr>
                <td>{{$b->id}}</td>
                <td>{{$b->author}}</td>
                <td>{{$b->judulartikel}}</td>
                <td>{{$b->isiartikel}}</td>
                <td>{{$b->created_at}}</td>
                <td>{{$b->updated_at}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

</body>

</html>