<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tekscontrol extends Model
{
    //
    protected $table = "tekscontrol";

    protected $fillable = ['file', 'nama', 'isiteks'];
}