<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class messagecontrol extends Model
{
    //
    protected $table = "message";

    protected $fillable = ['pengirim', 'isipesan'];
}