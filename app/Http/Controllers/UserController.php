<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

// use App\User;
use \DB;

class UserController extends Controller
{
    public function login()
    {
        return view('login');
    }

    // public function index()
    // {
    //     if (!Session::get('login')) {
    //         return redirect('login')->with('alert', 'Kamu harus login dulu');
    //     } else {
    //         return view('dashboard');
    //     }
    // }

    public function cekLogin(Request $request)
    {

        $this->validate($request,

            ['email' => 'required'],

            ['password' => 'required']

        );

        $email = $request->input('email');
        $pass = $request->input('password');

        $data = DB::table('admin')->where(['email' => $email])->first();
        if ($data != null) {
            if (($pass == $data->password) and ($data->email == $email)) {
                Session::put('email', $data->email);
                Session::put('login', true);
                $image = DB::table('imagescontrols')->get();
                $teks = DB::table('tekscontrol')->get();
                $portfolio = DB::table('portfolio')->paginate(5);

                return view('dashboard', ['image' => $image, 'teks' => $teks, 'portfolio' => $portfolio]);

            } else {
                return redirect('login')->with('alert', 'Password atau Email Salah!');
            }
        } else {
            return redirect('login')->with('alert', 'email belum terdaftar!');
        }

    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    public function tampil()
    {
        $data = DB::table('admin')->get();

        return view('userprofile', ['userprofile' => $data]);
    }

    public function edit($id)
    {
        $data = DB::table('admin')->where('id', $id)->get();

        return view('edituser', ['userprofile' => $data]);
    }

    public function update(Request $request)
    {
        DB::table('admin')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'tgl_lahir' => $request->tgl_lahir,
            'email' => $request->email,
            'password' => $request->password,
        ]);

        return redirect('profile')->with('status', 'Data Berhasil di Update');
    }

    public function tuser()
    {
        $data = DB::table('admin')->get();
        return view('tambahuser', ['userprofile' => $data]);
    }

    public function store(Request $request)
    {
        DB::table('admin')->insert([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'tgl_lahir' => $request->tgl_lahir,
            'email' => $request->email,
            'password' => $request->password,
        ]);

        return redirect('profile')->with('status', 'Admin Berhasil Ditambah');
    }

    public function destroy($id)
    {
        //
        DB::table('admin')->where('id', $id)->delete();

        return redirect('profile')->with('status', 'Admin Berhasil dihapus');
    }
}