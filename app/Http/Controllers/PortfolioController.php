<?php

namespace App\Http\Controllers;

use App\portfoliocontrol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class PortfolioController extends Controller
{
    //
    public function index()
    {
        $file = portfoliocontrol::get();

        return view('portfolio', ['file' => $file]);
    }

    public function cari(Request $request)
    {
        // menangkap data pencarian
        $cari = $request->cari;

        // mengambil data dari table sesuai pencarian
        $data = DB::table('portfolio')
            ->where('keterangan', 'like', "%" . $cari . "%")
            ->paginate();

        //mengirim data
        return view('portfolio', ['file' => $data]);
    }

    public function upload(Request $request)
    {
        $file = $request->file('file');

        $nama_file = time() . "_" . $file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $file->move(\base_path() . "/public/assets/img/portfolio", $nama_file);

        portfoliocontrol::create([
            'file' => $nama_file,
            'keterangan' => $request->keterangan,
        ]);

        return redirect('portfolio')->with('status', 'Portfolio Berhasil diupload');
    }

    public function edit($id)
    {
        $editportfolio = DB::table('portfolio')->where('id', $id)->get();

        return view('editportfolio', ['editportfolio' => $editportfolio]);
    }

    public function update(Request $request)
    {
        DB::table('portfolio')->where('id', $request->id)->update([
            'file' => $request->file,
            'keterangan' => $request->keterangan,
            'updated_at' => $request->updated_at,
        ]);

        return redirect('portfolio')->with('status', 'Portfolio Berhasil Di Update');
    }

    public function destroy($id)
    {
        DB::table('portfolio')->where('id', $id)->delete();

        return redirect('portfolio')->with('status', 'Data Berhasil dihapus');
    }

    public function cetak_pdf()
    {
        $portfolio = portfoliocontrol::all();

        $pdf = PDF::loadview('pdf_portfolio', ['portfolio' => $portfolio]);
        return $pdf->download('laporan-portfolio.pdf');
    }
}