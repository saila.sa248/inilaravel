<?php

namespace App\Http\Controllers;

use App\imagescontrol;
use DB;

class HomeController extends Controller
{
    //
    public function tampil()
    {
        $tampilslider = imagescontrol::get();
        $tampilteks = DB::table('tekscontrol')->get();
        $tampilportfolio = DB::table('portfolio')->get();

        return view('welcome', [
            'tampilslider' => $tampilslider,
            'tampilteks' => $tampilteks,
            'tampilportfolio' => $tampilportfolio,
        ]);
    }

    public function tblog()
    {
        $blog = DB::table('blogcontrol')->get();

        return view('blog', ['blog' => $blog]);
    }

    public function news1()
    {
        return view('news1');
    }
    public function news2()
    {
        return view('news2');
    }
    public function news3()
    {
        return view('news3');
    }
    public function news4()
    {
        return view('news4');
    }
    public function news5()
    {
        return view('news5');
    }
    public function news6()
    {
        return view('news6');
    }
    public function news7()
    {
        return view('news7');
    }
    public function news8()
    {
        return view('news8');
    }
    public function news9()
    {
        return view('news9');
    }
    public function news10()
    {
        return view('news10');
    }

}