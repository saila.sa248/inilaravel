<?php

namespace App\Http\Controllers;

use App\tekscontrol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //mengambil data dari table tekscontrol
        $data = tekscontrol::get();

        return view('controlteks', ['tekscontrol' => $data]);
    }

    public function cari(Request $request)
    {
        // menangkap data pencarian
        $cari = $request->cari;

        // mengambil data dari table sesuai pencarian
        $data = DB::table('tekscontrol')
            ->where('nama', 'like', "%" . $cari . "%")
            ->paginate();

        //mengirim data
        return view('controlteks', ['tekscontrol' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //insert data ke table tekscontrol
        $file = $request->file('file');

        $nama_file = time() . "_" . $file->getClientOriginalName();

        $file->move(\base_path() . "/public/assets/img/data_fileteks", $nama_file);

        tekscontrol::create([
            'file' => $nama_file,
            'nama' => $request->nama,
            'isiteks' => $request->isiteks,
            'created_at' => $request->date,
        ]);
        //alihkan halaman tambah data ke halaman controlteks
        return redirect('controlteks')->with('status', 'Data Berhasil di Tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $editteks = DB::table('tekscontrol')->where('id', $id)->get();

        return view('editteks', ['editteks' => $editteks]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //insert data ke table tekscontrol
        DB::table('tekscontrol')->where('id', $request->id)->update([
            'file' => $request->file,
            'nama' => $request->nama,
            'isiteks' => $request->isiteks,
            'updated_at' => $request->updated_at,
        ]);
        //alihkan halaman tambah data ke halaman controlteks
        return redirect('controlteks')->with('status', 'Data Berhasil di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('tekscontrol')->where('id', $id)->delete();

        return redirect('controlteks')->with('status', 'Data Berhasil dihapus');
    }
}