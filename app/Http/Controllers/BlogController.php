<?php

namespace App\Http\Controllers;

use App\blogcontrol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class BlogController extends Controller
{
    //
    public function index()
    {
        $data = blogcontrol::get();

        return view('controlblog', ['blogcontrol' => $data]);
    }

    public function cari(Request $request)
    {
        // menangkap data pencarian
        $cari = $request->cari;

        // mengambil data dari table sesuai pencarian
        $data = DB::table('blogcontrol')
            ->where('author', 'like', "%" . $cari . "%")
            ->orwhere('judulartikel', 'like', "%" . $cari . "%")
            ->paginate();

        //mengirim data
        return view('controlblog', ['blogcontrol' => $data]);
    }

    public function store(Request $request)
    {
        blogcontrol::create([
            'judulartikel' => $request->judulartikel,
            'isiartikel' => $request->isiartikel,
            'author' => $request->author,
        ]);

        return redirect('/blog')->with('status', 'Data Artikel Berhasil Ditambah');
    }

    public function edit($id)
    {
        $editblog = DB::table('blogcontrol')->where('id', $id)->get();

        return view('editblog', ['editblog' => $editblog]);
    }

    public function update(Request $request)
    {
        DB::table('blogcontrol')->where('id', $request->id)->update([
            'judulartikel' => $request->judulartikel,
            'isiartikel' => $request->isiartikel,
            'author' => $request->author,
            'updated_at' => $request->updated_at,
        ]);

        return redirect('/blog')->with('status', 'Data Berhasil di Update');
    }

    public function destroy($id)
    {
        DB::table('blogcontrol')->where('id', $id)->delete();

        return redirect('/blog')->with('status', 'Data Berhasil dihapus');
    }

    public function cetak_pdf()
    {
        $blog = blogcontrol::all();

        $pdf = PDF::loadview('pdf_blog', ['blog' => $blog]);
        return $pdf->download('laporan-artikelblog.pdf');
    }
}