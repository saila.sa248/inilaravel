<?php

namespace App\Http\Controllers;

use App\imagescontrol;
use DB;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index()
    {
        $data = DB::table('message')->get();

        return view('message', ['pesan' => $data]);
    }

    public function store(Request $request)
    {
        DB::table('message')->insert([
            'pengirim' => $request->pengirim,
            'isipesan' => $request->isipesan,
        ]);

        $tampilslider = imagescontrol::get();
        $tampilteks = DB::table('tekscontrol')->get();
        $tampilportfolio = DB::table('portfolio')->get();

        return view('welcome', [
            'tampilslider' => $tampilslider,
            'tampilteks' => $tampilteks,
            'tampilportfolio' => $tampilportfolio,
        ]);
    }

    public function cari(Request $request)
    {
        // menangkap data pencarian
        $cari = $request->cari;

        // mengambil data dari table sesuai pencarian
        $data = DB::table('message')
            ->where('pengirim', 'like', "%" . $cari . "%")
            ->paginate();

        //mengirim data
        return view('message', ['pesan' => $data]);
    }

    public function destroy($id)
    {
        DB::table('message')->where('id', $id)->delete();

        return redirect('/message')->with('status', 'Data Berhasil dihapus');
    }

    public function cetak_pdf()
    {
        $pesan = messagecontrol::all();

        $pdf = PDF::loadview('pdf_pesan', ['pesan' => $pesan]);
        return $pdf->download('laporan-pesan.pdf');
    }

}