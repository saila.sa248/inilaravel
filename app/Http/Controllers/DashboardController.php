<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    //
    public function tampil()
    {
        $image = DB::table('imagescontrols')->get();
        $teks = DB::table('tekscontrol')->get();
        $portfolio = DB::table('portfolio')->paginate(5);

        return view('dashboard', ['teks' => $teks, 'image' => $image, 'portfolio' => $portfolio]);

    }
}