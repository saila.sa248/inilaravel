<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Mail;

class ForgotPasswordController extends Controller
{
    //
    public function getEmail()
    {
        return view('password.email');
    }

    public function postEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:admin',
        ]);

        $token = Str::random(60);

        DB::table('password_resets')->insert(
            ['email' => $request->email, 'token' => $token, 'created_at' => carbon::now()]
        );

        Mail::send('password.verify', ['token' => $token], function ($message) use ($request) {
            $message->from($request->email);
            $message->to('saila.sa248@gmail.com');
            $message->subject('Reset Password Notification');
        });

        return back()->with('message', 'We Have e-mail your password reset link!');
    }
}