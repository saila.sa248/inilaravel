<?php

namespace App\Http\Controllers;

use App\imagescontrol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ImageController extends Controller
{
    //
    public function upload()
    {
        $gambar = imagescontrol::get();
        return view('uploadimage', ['gambar' => $gambar]);
    }

    public function proses_upload(Request $request)
    {

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('file');

        $nama_file = time() . "_" . $file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $file->move(\base_path() . "/public/assets/img", $nama_file);

        imagescontrol::create([
            'file' => $nama_file,
            'keterangan' => $request->keterangan,
        ]);

        return redirect('uploadimage')->with('status', 'Image Berhasil diupload');
    }

    public function edit($id)
    {
        $editimage = DB::table('imagescontrols')->where('id', $id)->get();

        return view('editimage', ['editimage' => $editimage]);
    }

    public function update(Request $request)
    {
        DB::table('imagescontrols')->where('id', $request->id)->update([
            'file' => $request->file,
            'keterangan' => $request->keterangan,
            'updated_at' => $request->updated_at,
        ]);

        return redirect('uploadimage')->with('status', 'Data Image Berhasil Di Update');
    }

    public function destroy($id)
    {
        //
        DB::table('imagescontrols')->where('id', $id)->delete();

        return redirect('uploadimage')->with('status', 'Data Image Berhasil dihapus');
    }

}