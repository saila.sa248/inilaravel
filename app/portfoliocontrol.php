<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class portfoliocontrol extends Model
{
    //
    protected $table = "portfolio";

    protected $fillable = ['file', 'keterangan'];
}