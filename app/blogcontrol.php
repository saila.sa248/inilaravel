<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class blogcontrol extends Model
{
    //
    protected $table = "blogcontrol";

    protected $fillable = ['judulartikel', 'isiartikel', 'author'];
}