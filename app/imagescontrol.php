<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class imagescontrol extends Model
{
    //
    protected $table = "imagescontrols";

    protected $fillable = ['file', 'keterangan'];
}