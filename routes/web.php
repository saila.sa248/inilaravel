<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// TAMPILAN UTAMA
Route::get('/', 'HomeController@tampil')->name('welcome');
Route::get('/halblog', 'HomeController@tblog')->name('halblog');
Route::get('/news1', 'HomeController@news1')->name('news1');
Route::get('/news2', 'HomeController@news2')->name('news2');
Route::get('/news3', 'HomeController@news3')->name('news3');
Route::get('/news4', 'HomeController@news4')->name('news4');
Route::get('/news5', 'HomeController@news5')->name('news5');
Route::get('/news6', 'HomeController@news6')->name('news6');
Route::get('/news7', 'HomeController@news7')->name('news7');
Route::get('/news8', 'HomeController@news8')->name('news8');
Route::get('/news9', 'HomeController@news9')->name('news9');
Route::get('/news10', 'HomeController@news10')->name('news10');

// LOGIN PAGE
Route::get('/login', 'UserController@login')->name('login')->middleware('guest');
Route::post('/signin', 'UserController@cekLogin')->name('ceklogin');
Route::get('/logout', 'UserController@logout')->name('logout');

// FORGOT PASSWORD PAGE
Route::get('forget-password', 'ForgotPasswordController@getEmail')->name('forgotpass');
Route::post('forget-password', 'ForgotPasswordController@postEmail')->name('forgotcek');
Route::get('reset-password/{token}', 'ResetPasswordController@getPassword');
Route::post('reset-password', 'ResetPasswordController@updatePassword')->name('resetpass');

// DASHBOARD PAGE
route::get('/dashboard', 'DashboardController@tampil')->name('dashboard');
Route::get('/header', 'DashboardController@tampiluser')->name('header');

// DATA TEKS PAGE
Route::get('/controlteks', 'TeksController@index')->name('controlteks');
Route::post('fileicon', 'TeksController@store');
Route::get('editteks/edit/{id}', 'TeksController@edit');
Route::post('/controlteks/update', 'TeksController@update')->name('update');
Route::get('tekscontrol/{tekscontrol}/destroy', [
    'uses' => 'TeksController@destroy',
    'as' => 'tekscontrol.destroy',
]);
Route::get('controlteks/cari', 'TeksController@cari')->name('cariteks');

// DATA IMAGE PAGE
Route::get('/uploadimage', 'ImageController@upload')->name('uploadimage');
Route::post('image', 'ImageController@proses_upload');
Route::get('editimage/edit/{id}', 'ImageController@edit');
Route::post('/image/update', 'ImageController@update')->name('updateimg');
Route::get('imagecontrol/{imagecontrol}/destroy', [
    'uses' => 'ImageController@destroy',
    'as' => 'imagecontrol.destroy',
]);

// DATA PORTFOLIO
Route::get('/portfolio', 'PortfolioController@index')->name('portfolio');
Route::post('portfolio', 'PortfolioController@upload');
Route::get('editportfolio/edit/{id}', 'PortfolioController@edit');
Route::post('/portfolio/update', 'PortfolioController@update')->name('upportfolio');
Route::get('portfolio/{portfolio}/destroy', [
    'uses' => 'PortfolioController@destroy',
    'as' => 'portfolio.destroy',
]);
Route::get('portfolio/cari', 'PortfolioController@cari')->name('cariportfolio');
Route::get('/portfolio/cetak_pdf', 'PortfolioController@cetak_pdf')->name('cetak_laporan');

// DATA BLOG
Route::get('/blog', 'BlogController@index')->name('blog');
Route::post('blog', 'BlogController@store');
Route::get('editblog/edit/{id}', 'BlogController@edit');
Route::post('/blog/update', 'BlogController@update')->name('upblog');
Route::get('blog/{blog}/destroy', [
    'uses' => 'BlogController@destroy',
    'as' => 'blogcontrol.destroy',
]);
Route::get('controlblog/cari', 'BlogController@cari')->name('cariartikel');
Route::get('/blog/cetak_pdf', 'BlogController@cetak_pdf')->name('cetak_laporan_blog');

// USER PROFILE
Route::get('/profile', 'UserController@tampil')->name('profile');
Route::get('edituser/edit/{id}', 'UserController@edit');
Route::post('/user/update', 'UserController@update')->name('upuser');
Route::post('user', 'UserController@store');
Route::get('/tambah', 'UserController@tuser')->name('tuser');
Route::get('user/{user}/destroy', [
    'uses' => 'UserController@destroy',
    'as' => 'usercontrol.destroy',
]);

// MESSAGE
Route::get('/message', 'MessageController@index')->name('message');
Route::post('kirimpesan', 'MessageController@store')->name('kirimpesan');
Route::get('message/{message}/destroy', [
    'uses' => 'MessageController@destroy',
    'as' => 'messagecontrol.destroy',
]);
Route::get('controlmessage/cari', 'MessageController@cari')->name('caripesan');
Route::get('/pesan/cetak_pdf', 'MessageController@cetak_pdf')->name('cetak_laporan_pesan');